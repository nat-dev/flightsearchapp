# FlightSearchApp

Flight Search App (Angular 6).

# How to run the project

+ npm install
	+ Packages should not give you any issues as specific versions are set in package.json to ensure that we all have the same experience when running the application.
+ npm start
	+ or *ng serve* as an alternative

# How to use the app

+ Create an account first
+ Log in with created account
+ You may now use the search form to search for flights
+ May also check weather forecast for arrival/destination points (but up to 5 days in the future, public API I'm using has this limit set)
+ May also visit /protected route to see the JWT token sent to a protected API action
	+ JWT token is only sent when needed, see my AuthInterceptor located in src/app/core/http/interceptors/)
	+ See Network tab in Chrome dev tools or any alternatives, the JWT token is attached as an Authorization header when making a call to the protected API controller under /protected route
	+ The JWT token is NOT present when making a request for logging in or registering as it's not needed
	+ The JWT token is NOT present when making a request for the public APIs being used in the app as that would not be secure

# Note!
+ The application makes use of 2 publicly available APIs (which may be slow at times)
	+ FlightSearch API: https://goibibo.3scale.net/docs
	+ Weather Forecast API: https://openweathermap.org/api