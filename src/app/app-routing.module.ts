import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './core/auth/auth.guard';

const routes: Routes = [
    { path: '', redirectTo: 'flights', pathMatch: 'full' },
    { path: 'accounts', loadChildren: './views/accounts/accounts.module#AccountsModule' },
    { path: 'flights', canActivate: [AuthGuard], loadChildren: './views/flights/flights.module#FlightsModule' },
    { path: 'protected', canActivate: [AuthGuard], loadChildren: './views/protected/protected.module#ProtectedModule' },
    { path: 'error', loadChildren: './views/errors/errors.module#ErrorsModule' },

    /* Place at end to handle unknown routes as any routes placed under this will be ignored. */
    { path: '**', redirectTo: 'error/404' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
