import { StorageKeysConstants } from '../constants/storage-keys.constants';
import { UserCredentials } from './user-credentials.interface';

export function getTokenFromStorage() {
    const credentials = getCredentialsFromStorage();
    if (credentials) {
        return credentials.token;
    }

    return null;
}


export function getCredentialsFromStorage() {
    const credentialsKey = StorageKeysConstants.CREDENTIALS_KEY;
    const credentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);

    if (credentials) {
        const savedCredentials: UserCredentials = JSON.parse(credentials);
        return savedCredentials;
    }

    return null;
}
