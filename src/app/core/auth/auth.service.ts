import { Injectable } from '@angular/core';

import { JwtHelperService } from '@auth0/angular-jwt';

import { StorageKeysConstants } from '../constants/storage-keys.constants';
import { getCredentialsFromStorage } from './auth.helper';
import { UserCredentials } from './user-credentials.interface';

const credentialsKey = StorageKeysConstants.CREDENTIALS_KEY;

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private credentials: UserCredentials | null;

    constructor() {
        this.credentials = getCredentialsFromStorage();
    }

    public setCredentials(credentials?: UserCredentials, remember?: boolean) {
        this.credentials = credentials || null;

        if (credentials) {
            const storage = remember ? localStorage : sessionStorage;
            storage.setItem(credentialsKey, JSON.stringify(credentials));
        } else {
            sessionStorage.removeItem(credentialsKey);
            localStorage.removeItem(credentialsKey);
        }
    }

    /**
     * Checks if the user is currently authenticated.
     * Returns true if user credentials are stored and if the token has not expired.
     */
    public isLoggedIn(): boolean {
        const jwtHelper = new JwtHelperService();

        if (this.credentials) {
            if (!jwtHelper.isTokenExpired(this.credentials.token)) {
                return true;
            }
            this.setCredentials(); // - Clear credentials if token has expired
        }

        return false;
    }

    public logout(): void {
        this.setCredentials();
    }

}
