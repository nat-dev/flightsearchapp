export class DateFormatConstants {

    /* Format of dates displayed in flights search results table */
    public static readonly FLIGHT_RESULTS = 'llll';
    /* Format of Arrival Date in flights search results table when departure and arrival are on the same date */
    public static readonly FLIGHT_RESULTS_SAME_ARRIVAL_DATE = 'h:mm A';

    /* Format of dates displayed in weather panel */
    public static readonly WEATHER_RESULTS = 'llll';

    /* Date format required by the search API to make a valid request */
    public static readonly FLIGHT_SEARCH_REQUEST = 'yyyyMMdd';
    /* Date format provided to moment to parse the custom format date response into a valid date object */
    public static readonly FLIGHT_SEARCH_PARSE_RESPONSE = 'yyyy-MM-DD\'t\'HHmm';

    public static readonly DATEPICKER_INPUT = 'll';
    public static readonly DATEPICKER_MONTH_YEAR_LABEL = 'MMM YYYY';
    public static readonly DATEPICKER_DATE_ACCESSIBLE_DATE_LABEL = 'LL';
    public static readonly DATEPICKER_DATE_ACCESSIBLE_MONTH_YEAR_LABEL = 'MMMM YYYY';

}
