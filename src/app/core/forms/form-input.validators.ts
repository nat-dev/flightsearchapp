import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export class FormInputValidators {

    /**
     * Simple validator to force user to select an option with material's autocomplete component
     * Based off: https://github.com/angular/material2/issues/3334#issuecomment-390231222
     */
    // public static requireAutocompleteOption(): ValidatorFn {
    //     return (control: AbstractControl): ValidationErrors => {
    //         const value = control.value;

    //         if (typeof value === 'string') {
    //             return { requireMatch: true };
    //         }

    //         return null;
    //     };
    // }

    public static requireAutocompleteOptions(options: string[]): ValidatorFn {
        return (control: AbstractControl): ValidationErrors => {
            const value = control.value;

            if (typeof value !== 'string' || !options.includes(value)) {
                return { requireMatch: true };
            }

            return null;
        };
    }

    public static valueEquals(valueEquals: any): ValidatorFn {
        return (control: AbstractControl): ValidationErrors => {
            const value = control.value;
            return value === valueEquals ? null : { notEqual: true };
        };
    }

}
