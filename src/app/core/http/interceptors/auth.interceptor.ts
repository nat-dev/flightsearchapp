import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { getTokenFromStorage } from '../../auth/auth.helper';

import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url = request.url;
        const whitelisted = environment.jwtWhitelistedUrls;
        const blacklisted = environment.jwtBlacklistedUrls;

        /* If URL is whitelisted AND NOT blacklisted */
        if (whitelisted.find(w => url.startsWith(w)) && !blacklisted.find(b => url.startsWith(b))) {
            const authRequest = request.clone({
                headers: request.headers.set('Authorization', `Bearer ${getTokenFromStorage()}`)
            });
            return next.handle(authRequest);
        }

        return next.handle(request);
    }

}
