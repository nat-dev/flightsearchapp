import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { ToastService } from '../../services/toast.service';

import { Observable } from 'rxjs/internal/Observable';
import { throwError } from 'rxjs/internal/observable/throwError';
import { catchError } from 'rxjs/internal/operators/catchError';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

    constructor(private toastService: ToastService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(error => this.errorHandler(error)));
    }

    private errorHandler(errorResponse: HttpErrorResponse): Observable<HttpEvent<any>> {
        if (!environment.production) {
            /* Can show all error details if env isn't production */
            console.log(`[${errorResponse.url}] ERROR:`, errorResponse);
        }

        /* 4XX Errors may be displayed to the user */
        if (errorResponse.status >= 400 && errorResponse.status < 500) {
            /* Error message is in error.message when returning Content() or in error when returning BadRequest() */
            /* A proper approach would be to create a set of structured response messages in the API project */
            const errorMessage = errorResponse.error.message || errorResponse.error;
            this.toastService.open(errorMessage);
            return throwError(errorMessage);
        }

        /* Other Errors (5XX, etc.) should not have their details displayed */
        this.toastService.open('Server Error');
        return throwError('Server Error');
    }

}
