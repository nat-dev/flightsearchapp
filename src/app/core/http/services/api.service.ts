import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/internal/Observable';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        private http: HttpClient
    ) { }

    public get<TResult>(route: string, params: {} = null): Observable<TResult> {
        return this.http.get<TResult>(route, { params: params });
    }

    public post<TPost, TResult = TPost>(route: string, object: TPost): Observable<TResult> {
        return this.http.post<TResult>(route, object);
    }

    public createHttpParams(params: {}): HttpParams {
        let httpParams: HttpParams = new HttpParams();
        Object.keys(params).forEach(param => {
            if (params[param]) {
                httpParams = httpParams.set(param, params[param]);
            }
        });

        return httpParams;
    }

}
