import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { ToastConfig } from '../../shared/models/toast/toast-config.model';

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    private readonly DEFAULT_CONFIG: ToastConfig = {
        horizontalPosition: 'end',
        verticalPosition: 'top',
        duration: 2000,
        panelClass: ['snackbar-notification']
    };

    constructor(private snackbar: MatSnackBar) { }

    public open(message: string, config?: ToastConfig): void {
        /*
            Keep DEFAULT_CONFIG values unless they are overridden for config? param,
            this way we can specify some values while keeping untouched values as default
        */
        config = { ...this.DEFAULT_CONFIG, ...config };

        this.snackbar.open(message, config.action, config);
    }

}
