import { FormGroup } from '@angular/forms';

export class BaseFormComponent {

    public form: FormGroup;

    /* If loader component should be rendered */
    public isLoading = false;

    /**
     * Checks if the form control with the provided name has the specified error.
     * @param controlName Name of the form control to check if error is present.
     * @param error The name of the error to check for.
     */
    public formControlHasError(controlName: string, error: string): boolean {
        return this.form.controls[controlName].hasError(error);
    }

    /**
     * Iterates through each FormControl of the provided form and marks
     * each one as touched.
     * @param form The form to mark it's controls as touched to trigger validation.
     */
    protected markFormControlsAsTouched(form: FormGroup): void {
        Object.keys(form.controls).forEach(field => {
            const control = form.get(field);
            control.markAsTouched({ onlySelf: true });
        });
    }
}
