import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

  @Input() public isLoading = false;
  @Input() public size = 3;
  @Input() public message: string;

  constructor() { }

}
