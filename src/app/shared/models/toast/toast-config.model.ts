import { MatSnackBarConfig } from '@angular/material/snack-bar';

export class ToastConfig extends MatSnackBarConfig {

    action?: string;

}
