import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MomentModule } from 'ngx-moment';

import { MaterialModule } from '../material.module';
import { LoaderComponent } from './components/loader/loader.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FlexLayoutModule,
        MomentModule
    ],
    exports: [
        CommonModule,
        MaterialModule,
        FlexLayoutModule,
        LoaderComponent,
        MomentModule
    ],
    declarations: [LoaderComponent]
})
export class SharedModule { }
