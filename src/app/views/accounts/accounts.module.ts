import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../shared/shared.module';
import { AccountsRoutingModule } from './accounts-routing.module';
import { AccountsComponent } from './accounts.component';
import { LogInComponent } from './log-in/log-in.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
    imports: [
        SharedModule,
        AccountsRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [
        AccountsComponent,
        LogInComponent,
        RegisterComponent
    ]
})
export class AccountsModule { }
