import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../../core/auth/auth.service';
import { UserCredentials } from '../../../core/auth/user-credentials.interface';
import { ToastService } from '../../../core/services/toast.service';
import { BaseFormComponent } from '../../../shared/components/_base/base-form.component';
import { LogInAccountResource } from '../../../shared/models/accounts/log-in-account-resource.model';
import { AccountsService } from '../services/accounts.service';

import { finalize } from 'rxjs/internal/operators/finalize';

@Component({
    selector: 'app-log-in',
    templateUrl: './log-in.component.html',
    styles: []
})
export class LogInComponent extends BaseFormComponent {

    public form = new FormGroup({
        username: new FormControl(null, Validators.required),
        password: new FormControl(null, Validators.required),
        rememberMe: new FormControl(false)
    });

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private accountsService: AccountsService,
        private authService: AuthService,
        private toastService: ToastService
    ) {
        super();
    }

    public onSubmit(): void {
        if (this.form.valid) {
            const rememberMe = this.form.value.rememberMe;
            const logInAccountResource: LogInAccountResource = {
                ...new LogInAccountResource(), ...this.form.value
            };

            this.isLoading = true;
            this.accountsService.logIn(logInAccountResource)
                .pipe(finalize(() => this.isLoading = false))
                .subscribe(
                    userCredentials => {
                        const credentials: UserCredentials = {
                            username: userCredentials.username,
                            token: userCredentials.token
                        };

                        this.authService.setCredentials(credentials, rememberMe);
                        this.toastService.open(`Welcome, ${credentials.username}!`);

                        /* Use returnUrl if available, otherwise use default route */
                        const targetUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
                        /* Copy queryParams so we can perform modifications before passing them to next route */
                        const queryParams = { ...this.route.snapshot.queryParams };
                        queryParams['returnUrl'] = null;

                        /* Navigate to targetUrl and add queryParams from route
                        use 'merge' to omit queryParams which are set to null (like returnUrl) after navigation */
                        this.router.navigate([targetUrl],
                            { queryParams: queryParams, queryParamsHandling: 'merge' }
                        );
                    }
                );
        } else {
            this.markFormControlsAsTouched(this.form);
        }
    }

}
