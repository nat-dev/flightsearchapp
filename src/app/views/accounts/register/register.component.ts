import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FormInputValidators } from '../../../core/forms/form-input.validators';
import { ToastService } from '../../../core/services/toast.service';
import { BaseFormComponent } from '../../../shared/components/_base/base-form.component';
import { RegisterAccountResource } from '../../../shared/models/accounts/register-account-resource.model';
import { AccountsService } from '../services/accounts.service';

import { finalize } from 'rxjs/internal/operators/finalize';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styles: []
})
export class RegisterComponent extends BaseFormComponent implements OnInit {

    public validationRules = {
        minLength: 2,
        maxLength: 100
    };

    public form = new FormGroup({
        name: new FormControl(null, [
            Validators.required,
            Validators.maxLength(this.validationRules.maxLength),
            Validators.minLength(this.validationRules.minLength)
        ]),
        surname: new FormControl(null, [
            Validators.required,
            Validators.maxLength(this.validationRules.maxLength),
            Validators.minLength(this.validationRules.minLength)
        ]),
        email: new FormControl(null, [
            Validators.required,
            Validators.maxLength(320), // - 320 chars should be max possible email length
            Validators.email
        ]),
        username: new FormControl(null, [
            Validators.required,
            Validators.maxLength(this.validationRules.maxLength),
            Validators.minLength(this.validationRules.minLength)
        ]),
        password: new FormControl(null, [
            Validators.required,
            Validators.pattern(`(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,${this.validationRules.maxLength}}`)
        ]),
        confirmPassword: new FormControl(null, [Validators.required])
    });


    constructor(
        private router: Router,
        private accountsService: AccountsService,
        private toastService: ToastService
    ) {
        super();
    }

    public ngOnInit(): void {
        this.onPasswordFormControlValueChanges();
    }

    /** When Password value changes, update the confirmPassword validator to be equal to the new password value */
    private onPasswordFormControlValueChanges(): void {
        this.form.get('password').valueChanges.subscribe(
            value => {
                const confirmPasswordFormControl = this.form.get('confirmPassword');
                confirmPasswordFormControl.setValidators([Validators.required, FormInputValidators.valueEquals(value)]);
                confirmPasswordFormControl.updateValueAndValidity();
            }
        );
    }

    public onSubmit(): void {
        if (this.form.valid) {
            const registerAccountResource: RegisterAccountResource = {
                ...new RegisterAccountResource(), ...this.form.value
            };

            this.isLoading = true;
            this.accountsService.registerAccount(registerAccountResource)
                .pipe(finalize(() => this.isLoading = false))
                .subscribe(
                    () => {
                        this.toastService.open('Your account has been created!');
                        this.router.navigate(['../']);
                    }
                );
        } else {
            this.markFormControlsAsTouched(this.form);
        }
    }

}
