import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { UserCredentials } from '../../../core/auth/user-credentials.interface';
import { ApiService } from '../../../core/http/services/api.service';
import { LogInAccountResource } from '../../../shared/models/accounts/log-in-account-resource.model';
import { RegisterAccountResource } from '../../../shared/models/accounts/register-account-resource.model';

import { Observable } from 'rxjs/internal/Observable';

@Injectable({
    providedIn: 'root'
})
export class AccountsService {

    private authEndpoint = `${environment.authApiEndpoint}/accounts`;

    constructor(
        private apiService: ApiService
    ) { }

    public registerAccount(registerAccountResource: RegisterAccountResource): Observable<void> {
        return this.apiService.post(`${this.authEndpoint}/register`, registerAccountResource);
    }

    public logIn(logInAccountResource: LogInAccountResource): Observable<UserCredentials> {
        return this.apiService.post(`${this.authEndpoint}/login`, logInAccountResource);
    }

}
