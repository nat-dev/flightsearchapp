import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ErrorsRoutingModule } from './errors-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
    imports: [
        SharedModule,
        ErrorsRoutingModule
    ],
    declarations: [NotFoundComponent]
})
export class ErrorsModule { }
