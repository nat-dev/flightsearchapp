import { Component } from '@angular/core';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
    styles: [`
        .message {
            font-size: 30rem;
        }
    `]
})
export class NotFoundComponent { }
