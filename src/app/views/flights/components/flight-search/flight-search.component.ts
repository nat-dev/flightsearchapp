import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import * as moment from 'moment';

import { DateFormatConstants } from '../../../../core/constants/date-format.constants';
import { FormInputValidators } from '../../../../core/forms/form-input.validators';
import { BaseFormComponent } from '../../../../shared/components/_base/base-form.component';
import { FlightsSearchQueryParams } from '../../models/flights/flight-search-query-params.model';
import { FlightSearchResultsDetails } from '../../models/flights/flight-search-results-details.interface';
import { FlightsSearchQuery } from '../../models/flights/request/flights-search-query.model';
import { FlightsService } from '../../services/flights.service';
import { AirportOption } from './models/airport-option.interface';

import { Observable } from 'rxjs/internal/Observable';
import { finalize } from 'rxjs/internal/operators/finalize';
import { map } from 'rxjs/internal/operators/map';
import { startWith } from 'rxjs/internal/operators/startWith';

@Component({
    selector: 'app-flight-search',
    templateUrl: './flight-search.component.html',
    styles: []
})
export class FlightSearchComponent extends BaseFormComponent implements OnInit {

    /* Search result details are emitted from this to the flights component to display the result */
    @Output() public searchComplete = new EventEmitter<FlightSearchResultsDetails>();

    /* Autocomplete options */
    public airports: AirportOption[] = [
        { code: 'CBG', name: 'Cambridge' },
        { code: 'FRA', name: 'Frankfurt' },
        { code: 'LHR', name: 'London' },
        { code: 'MLA', name: 'Luqa' },
        { code: 'MRS', name: 'Marseille' },
        { code: 'MXP', name: 'Milan' },
        { code: 'POX', name: 'Paris' },
        { code: 'SOU', name: 'Southampton' }
    ];
    public filteredSourceAirportOptions: Observable<AirportOption[]>;
    public filteredDestinationAirportOptions: Observable<AirportOption[]>;

    public form = new FormGroup({
        source: new FormControl(
            null, [Validators.required, FormInputValidators.requireAutocompleteOptions(this.airports.map(a => a.code))]
        ),
        destination: new FormControl(
            null, [Validators.required, FormInputValidators.requireAutocompleteOptions(this.airports.map(a => a.code))]
        ),
        adults: new FormControl(1, [Validators.required, Validators.min(1), Validators.max(9)]),
        children: new FormControl(0, [Validators.min(0), Validators.max(9)]),
        dateofdeparture: new FormControl(null, [Validators.required]),
        dateofarrival: new FormControl({ value: null, disabled: true })
    });
    public get dateOfDepartureFormControl(): AbstractControl {
        return this.form.get('dateofdeparture');
    }
    public get dateOfArrivalFormControl(): AbstractControl {
        return this.form.get('dateofarrival');
    }

    /* Datepicker */
    public datePicker = {
        inputExamples: {
            depDate:
                'Eg. ' + moment().format(DateFormatConstants.DATEPICKER_INPUT),
            arrDate:
                'Eg. ' + moment().add(5, 'days').format(DateFormatConstants.DATEPICKER_INPUT)
        },
        validation: {
            minDate: moment(),
            maxDate: moment().add(5, 'years')
        },
        format: DateFormatConstants.DATEPICKER_INPUT
    };

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private flightsService: FlightsService,
        private datePipe: DatePipe
    ) {
        super();
    }

    public ngOnInit(): void {
        this.initAutoCompleteControls();
        this.onDateOfDepartureValueChange();
        this.checkSearchQueryParams();
    }

    /** Checks for query params in the url and submits form if values are valid  */
    private checkSearchQueryParams(): void {
        if (this.route.snapshot.queryParamMap.keys.length > 0) {
            const queryInUrl = { ...new FlightsSearchQueryParams(), ...this.route.snapshot.queryParams };

            queryInUrl.dateofdeparture = moment(+queryInUrl.dateofdeparture);
            queryInUrl.dateofarrival = queryInUrl.dateofarrival ?
                moment(+queryInUrl.dateofarrival) : null;

            this.form.patchValue(queryInUrl);
            /* Check if form is valid to avoid submitting needless invalid forms if user touches query params */
            if (this.form.valid) {
                /* Manually call submit to replicate the same flow as form being submitted by the user */
                this.onSubmit();
            }
        }
    }

    public onSubmit(): void {
        if (this.form.valid) {
            const flightsSearchQuery: FlightsSearchQuery = { ...new FlightsSearchQuery(), ...this.form.value };

            /* Use ES Object Destructuring & Spread Operator to omit the properties I don't want to be in queryParams */
            const {
                app_id, app_key, counter, ...queryParams
            } = { ...flightsSearchQuery };

            /* Set search query params but stay on same route */
            this.router.navigate(['.'], { queryParams: queryParams });

            this.isLoading = true;
            this.formatSearchQueryDates(flightsSearchQuery);
            this.flightsService.searchFlights(flightsSearchQuery)
                .pipe(finalize(() => this.isLoading = false))
                .subscribe(
                    flightSearchResponse => {
                        const results = flightSearchResponse.data.onwardflights;
                        const searchResultDetails: FlightSearchResultsDetails = {
                            results: results,
                            /* City names needed to request Weather Queries */
                            sourceCity: this.getAirportNameByCode(flightsSearchQuery.source),
                            destinationCity: this.getAirportNameByCode(flightsSearchQuery.destination)
                        };
                        this.searchComplete.emit(searchResultDetails); // - Emit result details to the flights component
                    }
                );
        } else {
            this.markFormControlsAsTouched(this.form);
        }
    }

    /** Retrieves name of airport from AirportOptions using the code */
    private getAirportNameByCode(code: string): string {
        const airport = this.airports.find(a => a.code === code);
        return airport ? airport.name : null;
    }

    /** Transform dates to yyyyMMdd format to cater for the Flights Search API request */
    private formatSearchQueryDates(flightsSearchQuery: FlightsSearchQuery): void {
        flightsSearchQuery.dateofdeparture = this.datePipe.transform(flightsSearchQuery.dateofdeparture,
            DateFormatConstants.FLIGHT_SEARCH_REQUEST
        );
        flightsSearchQuery.dateofarrival = this.datePipe.transform(flightsSearchQuery.dateofarrival,
            DateFormatConstants.FLIGHT_SEARCH_REQUEST
        );
    }

    /** Sets dateofarrival form control to only be enabled if dateofdeparture form control has a value. */
    private onDateOfDepartureValueChange(): void {
        this.dateOfDepartureFormControl.valueChanges.subscribe(
            value => {
                if (value && this.dateOfDepartureFormControl.valid) {
                    this.dateOfArrivalFormControl.enable();
                } else {
                    this.dateOfArrivalFormControl.reset();
                    this.dateOfArrivalFormControl.disable();
                }
            }
        );
    }

    //#region Autocomplete
    /** Sets up filtering for the autocomplete form controls */
    private initAutoCompleteControls(): void {
        const filterOpts$ = (obs: Observable<any>) => {
            return obs.pipe(
                startWith(''),
                map(value => this.filterOptions(value))
            );
        };
        this.filteredSourceAirportOptions = filterOpts$(this.form.get('source').valueChanges);
        this.filteredDestinationAirportOptions = filterOpts$(this.form.get('destination').valueChanges);
    }

    /** Used by autocomplete to filter options */
    private filterOptions(value: string): AirportOption[] {
        const filterValue = value.toLowerCase();
        return this.airports.filter(option => option.name.toLowerCase().startsWith(filterValue));
    }

    //#endregion

}
