import { DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../../../shared/shared.module';
import { FlightSearchComponent } from './flight-search.component';

@NgModule({
    imports: [
        SharedModule,
        ReactiveFormsModule
    ],
    exports: [
        /* Export this component so it's visible to FlightsModule when importing this module */
        FlightSearchComponent
    ],
    providers: [
        /*
         * Using providers to avoid having to override the DatePipe
         * just to add an Injectable decorator and use providedIn
         */
        DatePipe
    ],
    declarations: [FlightSearchComponent]
})
export class FlightSearchModule { }
