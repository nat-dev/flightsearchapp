export interface AirportOption {
    code: string;
    name: string;
}
