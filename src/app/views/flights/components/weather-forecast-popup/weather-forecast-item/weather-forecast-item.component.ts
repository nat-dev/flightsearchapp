import { Component, Input } from '@angular/core';

import { WeatherForecastDetails } from '../../../models/weather/response/weather-forecast-details.interface';

@Component({
    selector: 'app-weather-forecast-item',
    templateUrl: './weather-forecast-item.component.html',
    styles: [`
        img.mat-list-image.mat-list-icon {
            width: 90px;
            height: 90px;
        }
    `]
})
export class WeatherForecastItemComponent {

    @Input() data: WeatherForecastDetails;

}
