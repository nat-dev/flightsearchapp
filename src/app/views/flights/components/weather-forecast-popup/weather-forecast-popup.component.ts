import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { WeatherForecastDialogData } from '../../models/weather/panel-data/weather-forecast-dialog-data.interface';

@Component({
    selector: 'app-weather-forecast-popup',
    templateUrl: './weather-forecast-popup.component.html',
    styleUrls: ['./weather-forecast-popup.component.scss']
})
export class WeatherForecastPopupComponent {

    constructor(
        public dialogRef: MatDialogRef<WeatherForecastPopupComponent>,
        @Inject(MAT_DIALOG_DATA) public forecasts: WeatherForecastDialogData
    ) { }

    public get isAnyForecastAvailable(): boolean {
        return !!(this.isDepartureForecastAvailable || this.isArrivalForecastAvailable);
    }

    public get isDepartureForecastAvailable(): boolean {
        return !!(this.forecasts.source.departure && this.forecasts.destination.departure);
    }

    public get isArrivalForecastAvailable(): boolean {
        return !!(this.forecasts.destination.arrival && this.forecasts.source.arrival);
    }

}
