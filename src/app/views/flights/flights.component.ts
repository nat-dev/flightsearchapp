import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

import * as moment from 'moment';

import { AuthService } from '../../core/auth/auth.service';
import { DateFormatConstants } from '../../core/constants/date-format.constants';
import { ToastService } from '../../core/services/toast.service';
import { WeatherForecastPopupComponent } from './components/weather-forecast-popup/weather-forecast-popup.component';
import { FlightSearchResultsDetails } from './models/flights/flight-search-results-details.interface';
import { FlightsSearchResult } from './models/flights/response/flights-search-result.interface';
import { WeatherForecastDialogData } from './models/weather/panel-data/weather-forecast-dialog-data.interface';
import { WeatherForecastDetails } from './models/weather/response/weather-forecast-details.interface';
import { WeatherForecastService } from './services/weather-forecast.service';

import { forkJoin } from 'rxjs/internal/observable/forkJoin';

@Component({
    selector: 'app-flights',
    templateUrl: './flights.component.html',
    styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {

    /* Contains values passed from flight-search component to query weather API with city names */
    public cityNames = {
        source: '',
        destination: ''
    };

    /* Table */
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort) private sort: MatSort;
    public dataSource = new MatTableDataSource<FlightsSearchResult>();
    public displayedColumns: string[] = [
        'airline', 'depdate', 'arrdate', 'stops', 'flightno', 'duration', 'weatherForecast'
    ];

    /* Ui */
    public isTableHidden = true;
    public backgroundImageNumber: number;
    public depDateFormat = DateFormatConstants.FLIGHT_RESULTS;
    public arrDateFormat(depDateTime: string, arrDateTime: string): string {
        const isSameDate = moment(depDateTime).isSame(moment(arrDateTime), 'day');
        return isSameDate ? DateFormatConstants.FLIGHT_RESULTS_SAME_ARRIVAL_DATE : DateFormatConstants.FLIGHT_RESULTS;
    }

    constructor(
        private router: Router,
        private authService: AuthService,
        private weatherForecastService: WeatherForecastService,
        private toastService: ToastService,
        private dialog: MatDialog
    ) { }

    public ngOnInit(): void {
        this.setRandomBackgroundImage();
        this.initTable();
    }

    /** Sets a randomly selected background image upon initialisation */
    private setRandomBackgroundImage(): void {
        const [min, max] = [1, 4];
        this.backgroundImageNumber = Math.floor(Math.random() * (max - min + 1) + min);
    }

    private initTable(): void {
        /* Table dataSource must be aware of paginator and sort objects to detect changes */
        [this.dataSource.paginator, this.dataSource.sort] = [this.paginator, this.sort];
        /* If the user changes the sort order, reset back to the first page */
        this.sort.sortChange.subscribe(() => this.paginator.firstPage());
    }

    public onSearchComplete(searchResultDetails: FlightSearchResultsDetails): void {
        this.cityNames.source = searchResultDetails.sourceCity;
        this.cityNames.destination = searchResultDetails.destinationCity;

        let results = searchResultDetails.results;
        if (results && results.length > 0) {
            this.isTableHidden = false;
            /* Map results to get the proper Arrival Date from the child flights (onwardflights) */
            results = results.map(r => {
                const stopsCount = r.stops;
                const onwardFlights = r.onwardflights;
                if (stopsCount && stopsCount > 0) {
                    // r.arrdate = onwardFlights[stopsCount - 1].arrdate;
                    r.arrdate = onwardFlights[onwardFlights.length - 1].arrdate;
                }

                r.depdate = moment(r.depdate, DateFormatConstants.FLIGHT_SEARCH_PARSE_RESPONSE).toDate();
                r.arrdate = moment(r.arrdate, DateFormatConstants.FLIGHT_SEARCH_PARSE_RESPONSE).toDate();

                return r;
            });

            this.dataSource.data = results;
            this.paginator.firstPage(); // - Reset page index after search results return
        } else {
            this.isTableHidden = true;
            this.dataSource.data = [];
            this.toastService.open('No results could be found. Try another search!', { action: 'Dismiss' });
        }
    }

    //#region Weather

    /**
     * Checks if the provided date is within the range between today + the number of days passed.
     * @param date The date to check if is after (now + number of days specified).
     * @param days The number of days to add to now.
     */
    public isWithinDays(date: Date, days: number): boolean {
        const dateAfterSpecifiedDays = moment().add(days, 'days');
        const dateToCheck = moment(date);

        if (dateToCheck.isAfter(dateAfterSpecifiedDays, 'days')) {
            return false;
        }

        return true;
    }

    /** Requests Weather API for Source/Destination using City Names */
    public onClickForecastAction(flight: FlightsSearchResult) {
        const sourceWeatherForecast$ = this.weatherForecastService.queryForecast(this.cityNames.source);
        const destinationWeatherForecast$ = this.weatherForecastService.queryForecast(this.cityNames.destination);

        forkJoin(sourceWeatherForecast$, destinationWeatherForecast$)
            .subscribe(
                ([sourceWeatherForecast, destinationWeatherForecast]) => {
                    const forecasts: WeatherForecastDialogData = {
                        /* Weather Forecast for SOURCE AIRPORT */
                        source: {
                            departure: this.findNearestDateForecast(sourceWeatherForecast.list, flight.depdate),
                            arrival: this.findNearestDateForecast(sourceWeatherForecast.list, flight.arrdate),
                            cityName: sourceWeatherForecast.city.name
                        },
                        /* Weather Forecast for DESTINATION AIRPORT */
                        destination: {
                            departure: this.findNearestDateForecast(destinationWeatherForecast.list, flight.depdate),
                            arrival: this.findNearestDateForecast(destinationWeatherForecast.list, flight.arrdate),
                            cityName: destinationWeatherForecast.city.name
                        }
                    };

                    /* Opens Weather Popup as a modal and passes forecasts as data */
                    this.dialog.open(WeatherForecastPopupComponent, {
                        data: forecasts
                    });
                }
            );
    }


    /** Takes in a list of weatherForecasts and a date and finds the nearest (by date and time) forecast */
    private findNearestDateForecast(forecasts: WeatherForecastDetails[], targetDate: Date): WeatherForecastDetails {
        let minDifference = Infinity;
        let nearestDateForecast: WeatherForecastDetails;

        // First filter out forecasts that are not on the same date
        const forecastLists = forecasts.filter(f => moment.unix(f.dt).isSame(moment(targetDate), 'day'));

        if (forecastLists.length === 0) {
            return null;
        }

        // Go through each forecast and calculate the difference between dates
        forecastLists.forEach(f => {
            const unixDate = moment.unix(f.dt);
            const currDifference = Math.abs(unixDate.diff(targetDate));
            if (currDifference < minDifference) {
                minDifference = currDifference;
                nearestDateForecast = f;
            }
        });

        // Update dt_txt to cater for time zone obtained using the unix timestamp
        // and format it before-hand to use for the weather panel
        nearestDateForecast.dt_txt = moment.unix(nearestDateForecast.dt).format(DateFormatConstants.WEATHER_RESULTS);

        return nearestDateForecast;
    }
    //#endregion

    public onLogOut(): void {
        this.authService.logout();
        this.router.navigate(['/accounts/login']);
    }

}
