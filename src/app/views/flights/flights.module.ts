import { NgModule } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { DateFormatConstants } from '../../core/constants/date-format.constants';
import { SharedModule } from '../../shared/shared.module';
import { FlightSearchModule } from './components/flight-search/flight-search.module';
import {
    WeatherForecastItemComponent,
} from './components/weather-forecast-popup/weather-forecast-item/weather-forecast-item.component';
import { WeatherForecastPopupComponent } from './components/weather-forecast-popup/weather-forecast-popup.component';
import { FlightsRoutingModule } from './flights-routing.module';
import { FlightsComponent } from './flights.component';
import { FlightStopsPipe } from './pipes/flight-stops.pipe';
import { KelvinToCelsiusPipe } from './pipes/kelvin-to-celsius.pipe';

@NgModule({
    imports: [
        SharedModule,
        FlightsRoutingModule,
        FlightSearchModule
    ],
    entryComponents: [
        /*
         * Material Modal must be specified as an entryComponent
         * See: https://github.com/angular/material2/issues/1491
         */
        WeatherForecastPopupComponent
    ],
    declarations: [
        FlightsComponent,
        WeatherForecastPopupComponent,
        FlightStopsPipe,
        KelvinToCelsiusPipe,
        WeatherForecastItemComponent
    ],
    providers: [
        /* Overrides datepicker date formats */
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        {
            provide: MAT_DATE_FORMATS,
            useValue: {
                parse: {
                    dateInput: DateFormatConstants.DATEPICKER_INPUT,
                },
                display: {
                    dateInput: DateFormatConstants.DATEPICKER_INPUT,
                    monthYearLabel: DateFormatConstants.DATEPICKER_MONTH_YEAR_LABEL,
                    dateA11yLabel: DateFormatConstants.DATEPICKER_DATE_ACCESSIBLE_DATE_LABEL,
                    monthYearA11yLabel: DateFormatConstants.DATEPICKER_DATE_ACCESSIBLE_MONTH_YEAR_LABEL,
                },
            }
        }
    ]
})
export class FlightsModule { }
