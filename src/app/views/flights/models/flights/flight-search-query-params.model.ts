import { Moment } from 'moment';

export class FlightsSearchQueryParams {

    source: string;
    destination: string;

    dateofdeparture: Moment;
    dateofarrival?: Moment;

    adults = 1;
    children?: number;

}
