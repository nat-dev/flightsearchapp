import { FlightsSearchResult } from './response/flights-search-result.interface';

export interface FlightSearchResultsDetails {
    results: FlightsSearchResult[];
    sourceCity: string;
    destinationCity: string;
}
