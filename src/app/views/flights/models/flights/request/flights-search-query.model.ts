import { FlightsSearchRequest } from './flights-search-request.abstract';

export class FlightsSearchQuery extends FlightsSearchRequest {

    source: string;
    destination: string;

    dateofdeparture: string; // YYYYMMDD Format Eg: 20181212 = 12 Dec 2018
    dateofarrival?: string;

    adults = 1;
    children?: number;

    counter: 0 | 100 = 100; // - Required for request (100 for domestic, 0 for international)
}
