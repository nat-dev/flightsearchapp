import { environment } from '../../../../../../environments/environment';

export abstract class FlightsSearchRequest {
    app_id = environment.externalAPIs.flightsSearch.id;
    app_key = environment.externalAPIs.flightsSearch.key;
}
