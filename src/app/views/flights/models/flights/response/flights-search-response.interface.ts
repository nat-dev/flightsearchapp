import { FlightsSearchResult } from './flights-search-result.interface';

export interface FlightsSearchResponse {
    data: { returnflights: FlightsSearchResult[], onwardflights: FlightsSearchResult[] };
    data_length: number;
}
