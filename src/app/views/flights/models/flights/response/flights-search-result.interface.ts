export interface FlightsSearchResult {
    airline: string;
    depdate: Date;
    arrdate: Date;
    stops: number;
    duration: Date;
    operatingcarrier: string;
    flightno: string;
    onwardflights: FlightsSearchResult[];
}
