import { WeatherForecastDetails } from '../response/weather-forecast-details.interface';

export interface WeatherForecastDialogDataRow {
    departure: WeatherForecastDetails;
    arrival: WeatherForecastDetails;
    cityName: string;
}
