import { WeatherForecastDialogDataRow } from './weather-forecast-dialog-data-row.interface';

export interface WeatherForecastDialogData {
    source: WeatherForecastDialogDataRow;
    destination: WeatherForecastDialogDataRow;
}
