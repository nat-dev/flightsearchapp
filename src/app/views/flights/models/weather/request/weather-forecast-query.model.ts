import { WeatherForecastRequest } from './weather-forecast-request.abstract';

export class WeatherForecastQuery extends WeatherForecastRequest {

    /* q city name and country code divided by comma, use ISO 3166 country codes */
    q: string;

    constructor(q: string) {
        super();
        this.q = q;
    }

}
