import { environment } from '../../../../../../environments/environment';

export abstract class WeatherForecastRequest {
    appId = environment.externalAPIs.weatherForecast.key;
}
