import { WeatherForecastDetails } from './weather-forecast-details.interface';

export interface WeatherForecastResponse {
    cod: string;
    message: number;
    cnt: number;
    list: WeatherForecastDetails[];
    city: {
        id: number,
        name: string,
        coord: {
            lat: number,
            long: number
        },
        country: string
    };
}
