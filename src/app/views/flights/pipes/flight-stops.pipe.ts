import { Pipe, PipeTransform } from '@angular/core';

import { FlightListConstants } from '../../../core/constants/flight-list-config.constants';

@Pipe({
    name: 'flightStops'
})
export class FlightStopsPipe implements PipeTransform {

    public transform(value: number): string {
        if (value && value > 0) {
            return `${value} ${FlightListConstants.FLIGHT_STOPS_APPEND}`;
        }

        return FlightListConstants.FLIGHT_STOPS_NONE;
    }

}
