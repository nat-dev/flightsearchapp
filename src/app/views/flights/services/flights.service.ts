import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { ApiService } from '../../../core/http/services/api.service';
import { FlightsSearchQuery } from '../models/flights/request/flights-search-query.model';
import { FlightsSearchResponse } from '../models/flights/response/flights-search-response.interface';

import { Observable } from 'rxjs/internal/Observable';

@Injectable({
    providedIn: 'root'
})
export class FlightsService {

    private flightsSearchEndpoint = environment.externalAPIs.flightsSearch.url;

    constructor(
        private apiService: ApiService
    ) { }

    public searchFlights(searchQuery: FlightsSearchQuery): Observable<FlightsSearchResponse> {
        const params = this.apiService.createHttpParams(searchQuery);
        return this.apiService.get(this.flightsSearchEndpoint, params);
    }

}
