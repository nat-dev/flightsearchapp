import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { ApiService } from '../../../core/http/services/api.service';
import { WeatherForecastQuery } from '../models/weather/request/weather-forecast-query.model';
import { WeatherForecastResponse } from '../models/weather/response/weather-forecast-response.interface';

import { Observable } from 'rxjs/internal/Observable';

@Injectable({
    providedIn: 'root'
})
export class WeatherForecastService {

    private weatherForecastEndpoint = environment.externalAPIs.weatherForecast.url;

    constructor(
        private apiService: ApiService
    ) { }

    public queryForecast(query: string): Observable<WeatherForecastResponse> {
        const queryObj = new WeatherForecastQuery(query);
        const params = this.apiService.createHttpParams(queryObj);

        return this.apiService.get(this.weatherForecastEndpoint, params);
    }

}
