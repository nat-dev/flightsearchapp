import { Component, OnInit } from '@angular/core';

import { ProtectedService } from './services/protected.service';

import { finalize } from 'rxjs/internal/operators/finalize';

@Component({
    selector: 'app-protected',
    templateUrl: './protected.component.html',
    styles: []
})
export class ProtectedComponent implements OnInit {

    public isLoading = false;
    public response: string;

    constructor(
        private protectedService: ProtectedService
    ) { }

    ngOnInit() {
        this.isLoading = true;
        this.protectedService.testAuth()
            .pipe(finalize(() => this.isLoading = false))
            .subscribe(
                resp => {
                    this.response = resp;
                }
            );
    }

}
