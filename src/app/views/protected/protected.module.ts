import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ProtectedRoutingModule } from './protected-routing.module';
import { ProtectedComponent } from './protected.component';

@NgModule({
    imports: [
        SharedModule,
        ProtectedRoutingModule
    ],
    declarations: [ProtectedComponent]
})
export class ProtectedModule { }
