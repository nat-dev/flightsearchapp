import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { ApiService } from '../../../core/http/services/api.service';

import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProtectedService {
    private endPoint = `${environment.authApiEndpoint}/protected`;

    constructor(
        private apiService: ApiService
    ) { }

    public testAuth(): Observable<string> {
        return this.apiService.get(this.endPoint);
    }

}
