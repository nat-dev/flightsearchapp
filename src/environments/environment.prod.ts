export const environment = {
    production: true,
    externalAPIs: {
        flightsSearch: {
            url: 'http://developer.goibibo.com/api/search/',
            id: '1ce0c83f',
            key: '1f7f761ce461e8f040d50b363b5a4d1c'
        },
        weatherForecast: {
            url: 'http://api.openweathermap.org/data/2.5/forecast',
            key: '99d23c914d14e3af01422fba81126748'
        }
    },
    authApiEndpoint: `http://api.nat-flight-search.com/api`,
    jwtWhitelistedUrls: ['http://api.nat-flight-search.com/api'],
    jwtBlacklistedUrls: ['http://api.nat-flight-search.com/api/accounts']
};
