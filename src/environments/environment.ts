// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    externalAPIs: {
        flightsSearch: {
            url: 'http://developer.goibibo.com/api/search/',
            id: '1ce0c83f',
            key: '1f7f761ce461e8f040d50b363b5a4d1c'
        },
        weatherForecast: {
            url: 'http://api.openweathermap.org/data/2.5/forecast',
            key: '99d23c914d14e3af01422fba81126748'
        }
    },
    authApiEndpoint: `http://localhost:5001/api`,
    /* URLs that will have Bearer Token attached as an Authorization Header */
    jwtWhitelistedUrls: ['http://localhost:5001/api'],
    /**
     * URLs that will NOT have Bearer Token attached as an Authorization Header
     * Useful for specifying unprotected routes present in jwtWhitelistedUrls like login/register routes
     */
    jwtBlacklistedUrls: ['http://localhost:5001/api/accounts']
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
